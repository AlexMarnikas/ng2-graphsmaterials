import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { LinearGraphComponent } from '../app/linear-graph/linear-graph.component';
import { Component, Input } from '@angular/core';
import { PieChartComponent } from '../app/pie-chart/pie-chart.component';
import { DoughnutChartComponent } from '../app/doughnut-chart/doughnut-chart.component';
import { PolarAreaChartComponent } from '../app/polar-area-chart/polar-area-chart.component';
import { RadarChartComponent } from '../app/radar-chart/radar-chart.component';

import {ChartsModule} from 'ng2-charts/components/charts/charts';

import { MaterialModule } from '@angular/material';

import { ButtonComponent } from '../app/button/button.component';
import { CheckboxComponent } from '../app/checkbox/checkbox.component';
import {SidenavComponent} from '../app/sidenav/sidenav.component';
import { MenuComponent }   from '../app/menu/menu.component';
import { ProgressBarComponent }   from '../app/progress-bar/progress-bar.component';
import { ProgressCircleComponent }   from '../app/progress-circle/progress-circle.component';

import { RouterModule }   from '@angular/router';





@NgModule({
  declarations: [
    AppComponent
    ,LinearGraphComponent
    ,PieChartComponent
    ,DoughnutChartComponent
    ,PolarAreaChartComponent
    ,RadarChartComponent
    ,ButtonComponent
    ,CheckboxComponent
    ,SidenavComponent
    ,MenuComponent
    ,ProgressBarComponent
    ,ProgressCircleComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
    ,ChartsModule
    ,MaterialModule.forRoot(),
    RouterModule.forRoot([
        
          {path: 'button', component: ButtonComponent},
          {path: 'checkbox' , component: CheckboxComponent}
        
])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
