import { Component } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})

export class ButtonComponent {
  title = 'Button Components!';
  isDisabled: boolean = false;
  clickCounter: number = 0;
}
