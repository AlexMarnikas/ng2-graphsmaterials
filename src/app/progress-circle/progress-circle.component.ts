import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-progress-circle',
  templateUrl: './progress-circle.component.html',
  styleUrls: ['./progress-circle.component.css']
})

export class ProgressCircleComponent {
  title="Progress Circle";
  progressValue: number = 40;

  step(val: number) {
    this.progressValue += val;
  }

}

