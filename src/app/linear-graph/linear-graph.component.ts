import { Component,NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
 

@Component({
  selector: 'app-linear-graph',
  templateUrl: './linear-graph.component.html',
  styleUrls: ['./linear-graph.component.css']
})
  
export class LinearGraphComponent {
     private datasets = [
    {
      data: [225.70000000000002, 218.9, 534.8000000000001, 211.4, 213.10000000000002, 216.20000000000002, 212.60000000000002, 211.5, 217.70000000000002, 214.60000000000002, 215.60000000000002, 213.10000000000002, 214.4, 211.2, 210.60000000000002, 210.9, 216.29999999999998, 227.89999999999998, 486.09999999999997, 482.8, 537.3, 545.2, 237.2, 237.8, 228.2]
    }
  ];

  private labels = ["15", "16", "17", "18", "19", "20", "21", "22", "23", "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15"];

  private options = {
    legend: {
      display: false,
    },
    scales: {
        xAxes: [{
          ticks: {
            callback: function(dataLabel, index) {
              return index % 3 === 0 ? dataLabel : '';
            }
          },
          scaleLabel: {
            display: true,
            labelString: 'hour'
          }
        }],
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  };

}
