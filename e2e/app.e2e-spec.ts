import { Ng2GraphsPage } from './app.po';

describe('ng2-graphs App', function() {
  let page: Ng2GraphsPage;

  beforeEach(() => {
    page = new Ng2GraphsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
